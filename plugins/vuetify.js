import Vue from 'vue'
import Vuetify from 'vuetify'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: '#207EFF',
    secondary: '#f3f4f7',
    accent: '#9c27b0',
    error: '#f44336',
    warning: '#ffd100',
    info: '#2196f3',
    success: '#4caf50'
  }
})
