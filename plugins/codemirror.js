// require lib
import Vue from 'vue'
import VueCodemirror from 'vue-codemirror'

import '~/node_modules/codemirror/mode/xml/xml.js'
import '~/node_modules/codemirror/addon/wrap/hardwrap.js'

Vue.use(VueCodemirror)
