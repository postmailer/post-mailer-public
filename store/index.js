import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      activeWidget: null,
      activeBlock: null,
      widgetOptions: null,
      panel: [false, true],
      error: '',
      notification: '',
      saved: false,
      build: {
        name: '',
        blocks: [{ widgets: [] }]
      },
      fileProgress: 0,
      options: {}
    },
    mutations: {
      initBuild(state) {
        let build = localStorage.getItem('nf-build')
        build = JSON.parse(build)

        if (build) state.build = build
      },
      updateBlock(state, blocks) {
        state.build.blocks = blocks
      },
      updateWidget(state, { index, widgets }) {
        state.build.blocks[index].widgets = widgets
      },
      addBlock(state) {
        const block = {
          widgets: [{ active: false }]
        }
        state.build.blocks.push(block)
      },
      deleteBlock(state, index) {
        state.build.blocks.splice(index, 1)
      },
      setActive(state, { index, blockIndex, options }) {
        state.activeWidget = index
        state.activeBlock = blockIndex

        if (options) {
          state.widgetOptions = options
          state.panel = [true, true]
          state.options = {}
        }
      },
      removeWidget(state, { index, blockIndex }) {
        const widgets = state.build.blocks[blockIndex].widgets

        state.activeBlock = null
        state.activeWidget = null
        state.widgetOptions = null
        state.panel = [false, true]

        widgets.splice(index, 1)
      },
      updateOption(state, { options, type }) {
        const block = state.build.blocks[state.activeBlock]
        const widget = block.widgets[state.activeWidget]
        const newOption = { [type]: options }

        if (widget.options !== null) {
          widget.options[type] = options
        } else {
          widget.options = newOption
        }

        state.options = newOption
      },
      setOptions(state, options) {
        const block = state.build.blocks[state.activeBlock]
        const widget = block.widgets[state.activeWidget]

        widget.options = options
      },
      fileProgress(state, payload) {
        state.fileProgress = payload
      },
      updateText(state, text) {
        const block = state.build.blocks[state.activeBlock]
        if (Object.keys(block.widgets).length > 0) {
          const widget = block.widgets[state.activeWidget]
          widget.text = text
        }
      },
      setError(state, message) {
        state.error = message
      },
      setNotification(state, message) {
        state.error = message
      },
      clearMessage(state) {
        state.error = ''
        state.notification = ''
      }
    },
    actions: {
      addBlock({ commit }) {
        commit('addBlock')
      },
      deleteBlock({ commit }, order) {
        commit('deleteBlock', order)
      },
      setActive({ commit }, payload) {
        commit('setActive', payload)
      },
      removeWidget({ commit }, payload) {
        commit('removeWidget', payload)
      },
      updateOption({ commit }, payload) {
        commit('updateOption', payload)
      },
      setOptions({ commit }, options) {
        commit('setOptions', options)
      },
      fileProgress({ commit }, payload) {
        commit('fileProgress', payload)
      },
      updateText({ commit }, payload) {
        commit('updateText', payload)
      },
      setError({ commit }, message) {
        commit('setError', message)
      },
      setNotification({ commit }, message) {
        commit('setError', message)
      },
      clearMessage({ commit }) {
        commit('clearMessage')
      }
    },
    getters: {
      getCurrentOptions: state => {
        return Object.keys(state.options).length > 0
          ? state.options
          : state.widgetOptions
      }
    }
  })
}

export default createStore
