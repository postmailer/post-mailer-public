import Wrapper from './Wrapper.vue'

export default {
  render: function(h) {
    return h(Wrapper, this.$slots.default)
  }
}
