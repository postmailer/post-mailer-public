export default {
  image: {
    label: 'Загрузить изображение',
    value: '',
    default:
      'https://marketing-image-production.s3.amazonaws.com/uploads/6f0ca564d8f266855a685b93cdd0b9bcef1f49236480573fa5f0c797d218dd5d96bb5e19f97d70a448ff55a3950c66a9932932ac345eecd9e96e02529481b1b3.jpg'
  },
  padding: {
    label: 'Отступы',
    value: [0, 0],
    default: [0, 0]
  }
}
