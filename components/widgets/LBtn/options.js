export default {
  width: {
    label: 'Ширина',
    value: 0,
    default: 100
  },
  textField: {
    label: 'Текст кнопки',
    value: '',
    default: 'Найти клиентов'
  },
  link: {
    label: 'Ссылка',
    value: '',
    default: 'https://newton.finance/'
  }
}
