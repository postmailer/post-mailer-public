import firebase from "firebase";

firebase.initializeApp({
  apiKey: "AIzaSyAKbeVxmNH2XJvPPFhUBicVdUR4ki4n3zI",
  authDomain: "post-mailer.firebaseapp.com",
  databaseURL: "https://post-mailer.firebaseio.com",
  projectId: "post-mailer",
  storageBucket: "post-mailer.appspot.com",
  messagingSenderId: "581831820104"
});

const db = firebase.firestore();
const storage = firebase.storage();

function uploadFile(file, res, error, cb) {
  const fileData = storage.ref().child(`widgets/${file.name}`);
  const uploadTask = fileData.put(file);
  uploadTask.on(
    "state_changed",
    snapshot => {
      const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      res(progress);
    },
    e => error(e.message),
    () => {
      uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
        const src = downloadURL;
        cb(src);
      });
    }
  );
}

export default {
  db,
  storage,
  uploadFile
};
